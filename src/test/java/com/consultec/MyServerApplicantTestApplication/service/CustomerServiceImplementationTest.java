package com.consultec.MyServerApplicantTestApplication.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.consultec.MyServerApplicantTestApplication.dao.CustomerDao;
import com.consultec.MyServerApplicantTestApplication.model.Customer;
import com.consultec.MyServerApplicantTestApplication.model.Status;

public class CustomerServiceImplementationTest {

  @Mock
  private CustomerDao customerDao;

  @InjectMocks
  private CustomerServiceImplementation customerServiceImplementation;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void CustomerServiceCreateTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    customerServiceImplementation.saveCustomer(customer);
    assertNotNull(customer.getId());
  }

  @Test
  public void CustomerServiceDeleteTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    customerServiceImplementation.deleteCustomer(customer.getId());
    customer = null;
    assertNull(customer);
  }

  @Test
  public void CustomerServiceUpdateTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    customerServiceImplementation.updateCustomer(customer);
    assertNotNull(customer.getId());
  }

  @Test
  public void CustomerServiceFindAllCustomerTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    List<Customer> customers = new ArrayList<>();
    customers.add(customer);
    customerServiceImplementation.findAllCustomer();
    assertEquals(customers.size(), 1);
    assertNotNull(customers);
  }

  @Test
  public void CustomerServiceFindAllCustomerByUsernameTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    List<Customer> customers = new ArrayList<>();
    customers.add(customer);
    customerServiceImplementation.findCustomerByUserNameAndStatus(customer.getUsername(),
        Status.ENABLED.toString());
    assertEquals(customers.size(), 1);
    assertNotNull(customers);
  }

  @Test
  public void CustomerServiceFindAllCustomerByIdTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    customerServiceImplementation.findCustomerById(customer.getId());
    assertEquals(customer.getId(), 1);
    assertNotNull(customer.getId());
  }
}
