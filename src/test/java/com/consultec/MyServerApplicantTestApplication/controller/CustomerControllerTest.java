package com.consultec.MyServerApplicantTestApplication.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.consultec.MyServerApplicantTestApplication.model.Customer;
import com.consultec.MyServerApplicantTestApplication.model.Status;
import com.consultec.MyServerApplicantTestApplication.service.CustomerService;


public class CustomerControllerTest {

  private MockMvc mockMvc;

  @Mock
  private CustomerService customerService;

  @InjectMocks
  private CustomerController customerController;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
    mockMvc = MockMvcBuilders.standaloneSetup(customerController).addFilters().build();
  }

  @Test
  public void testCreateCustomer() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    String customerJson = "{ \"name\":\"victor\",\n" + "    \"lastName\":\"tejada yau\",\n"
        + "    \"username\":\"victoryau\", \n" + "    \"password\":\"Panama\", \n"
        + "    \"email\": \"victortyau@gmail.com\",\n" + "    \"address\":\"Panama city\",\n"
        + "    \"status\": \"DISABLED\"  }";
    doNothing().when(customerService).saveCustomer(customer);
    mockMvc
        .perform(
            post("/v1/customers").contentType(MediaType.APPLICATION_JSON).content(customerJson))
        .andExpect(status().isCreated());
  }
  
  @Test
  public void testDeleteCustomer() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    when(customerService.findCustomerById(customer.getId())).thenReturn(customer);
    doNothing().when(customerService).deleteCustomer(customer.getId());
    
    mockMvc.perform(delete("/v1/customers/{id}", customer.getId())).andExpect(status().isNoContent());
    
  }
  
  @Test
  public void testDeleteCustomerNull() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    when(customerService.findCustomerById(customer.getId())).thenReturn(null);
    
    mockMvc.perform(delete("/v1/customers/{id}", customer.getId())).andExpect(status().isNotFound());
  }
  
  @Test
  public void testUpdateCustomer() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    String customerJson = "{ \"name\":\"victor\",\n" + "    \"lastName\":\"tejada yau\",\n"
        + "    \"username\":\"victoryau\", \n" + "    \"password\":\"Panama\", \n"
        + "    \"email\": \"victortyau@gmail.com\",\n" + "    \"address\":\"Panama city\",\n"
        + "    \"status\": \"DISABLED\"  }";
    when(customerService.findCustomerById(customer.getId())).thenReturn(customer);
    doNothing().when(customerService).updateCustomer(customer);
    mockMvc
        .perform(
            patch("/v1/customers/{id}",customer.getId()).contentType(MediaType.APPLICATION_JSON).content(customerJson))
        .andExpect(status().isOk());
  }
  
  @Test
  public void testUpdateCustomerNull() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    String customerJson = "{ \"name\":\"victor\",\n" + "    \"lastName\":\"tejada yau\",\n"
        + "    \"username\":\"victoryau\", \n" + "    \"password\":\"Panama\", \n"
        + "    \"email\": \"victortyau@gmail.com\",\n" + "    \"address\":\"Panama city\",\n"
        + "    \"status\": \"DISABLED\"  }";
    when(customerService.findCustomerById(customer.getId())).thenReturn(null);
    mockMvc
        .perform(
            patch("/v1/customers/{id}",customer.getId()).contentType(MediaType.APPLICATION_JSON).content(customerJson))
        .andExpect(status().isNotFound());
  }
  
  @Test
  public void testFindAllCustomer() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    List<Customer> customers = new ArrayList<>();
    customers.add(customer);
    
    when(customerService.findAllCustomer()).thenReturn(customers);
    mockMvc.perform(get("/v1/customers")).andExpect(status().isOk());
  }
  
  @Test
  public void testFindAllCustomerEmpty() throws Exception {
    List<Customer> customers = new ArrayList<>();
    
    when(customerService.findAllCustomer()).thenReturn(customers);
    mockMvc.perform(get("/v1/customers")).andExpect(status().isNoContent());
  }
  
  @Test
  public void testFindCustomerById() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    
    when(customerService.findCustomerById(customer.getId())).thenReturn(customer);
    mockMvc.perform(get("/v1/customers/{id}", customer.getId())).andExpect(status().isOk());
  }
  
  @Test
  public void testFindCustomerByIdNull() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    
    when(customerService.findCustomerById(customer.getId())).thenReturn(null);
    mockMvc.perform(get("/v1/customers/{id}", customer.getId())).andExpect(status().isNotFound());
  }
  
  @Test
  public void testFindCustomerByUsernameAndStatus() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    List<Customer> customers = new ArrayList<>();
    customers.add(customer);
    
    when(customerService.findCustomerByUserNameAndStatus(customer.getUsername(), Status.ENABLED.toString())).thenReturn(customers);
    mockMvc.perform(get("/v1/customers/username").param("username", customer.getUsername()).param("status", Status.ENABLED.toString())).andExpect(status().isOk());
  }
  
  @Test
  public void testFindCustomerByUsernameAndStatusEmpty() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    List<Customer> customers = new ArrayList<>();
    
    when(customerService.findCustomerByUserNameAndStatus(customer.getUsername(), Status.ENABLED.toString())).thenReturn(customers);
    mockMvc.perform(get("/v1/customers/username").param("username", customer.getUsername()).param("status", Status.ENABLED.toString())).andExpect(status().isNoContent());
  }
}