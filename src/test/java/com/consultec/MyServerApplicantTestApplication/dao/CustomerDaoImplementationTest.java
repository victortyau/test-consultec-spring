package com.consultec.MyServerApplicantTestApplication.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.consultec.MyServerApplicantTestApplication.model.Customer;
import com.consultec.MyServerApplicantTestApplication.model.Status;

public class CustomerDaoImplementationTest {

  @Mock
  private EntityManager entityManager;

  @Mock
  private Session session;

  @InjectMocks
  private CustomerDaoImplementation customerDaoImplementation;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
    when(entityManager.unwrap(Session.class)).thenReturn(session);
  }

  @Test
  public void CustomerDaoImplementationCreateTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    customerDaoImplementation.saveCustomer(customer);
    assertNotNull(customer.getId());
  }

  @Test
  public void CustomerDaoImplementationDeleteTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    when(customerDaoImplementation.findCustomerById(customer.getId())).thenReturn(customer);
    customerDaoImplementation.deleteCustomer(customer.getId());
    customer = null;
    assertNull(customer);
  }

  @Test
  public void CustomerDaoImplementationDeleteTestNull() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    when(customerDaoImplementation.findCustomerById(customer.getId())).thenReturn(null);
    customerDaoImplementation.deleteCustomer(customer.getId());
    customer = null;
    assertNull(customer);
  }

  @Test
  public void CustomerDaoImplementationUpdateTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    customerDaoImplementation.updateCustomer(customer);
    assertNotNull(customer.getId());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void CustomerDaoImplementationFindAllCustomerTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    List<Customer> customers = new ArrayList<>();
    customers.add(customer);
    Query<Customer> query = Mockito.mock(Query.class);
    when(session.createQuery("from Customer")).thenReturn(query);
    when(query.list()).thenReturn(customers);
    customerDaoImplementation.findAllCustomer();
    assertEquals(customers.size(), 1);
  }
  
  
  @SuppressWarnings("unchecked")
  @Test
  public void CustomerDaoImplementationFindCustomerByUsernameAndStatusEnabledTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.ENABLED);
    List<Customer> customers = new ArrayList<>();
    customers.add(customer);
    Query<Customer> query = Mockito.mock(Query.class);
    when(session.createQuery("from Customer where username = :username and status = :status")).thenReturn(query);
    when(query.setParameter("username", customer.getUsername())).thenReturn(query);
    when(query.setParameter("status", Status.ENABLED)).thenReturn(query);
    when(query.list()).thenReturn(customers);
    customerDaoImplementation.findCustomerByUserNameAndStatus(customer.getUsername(), Status.ENABLED.toString());
    assertEquals(customers.size(), 1);
  }
  
  @SuppressWarnings("unchecked")
  @Test
  public void CustomerDaoImplementationFindCustomerByUsernameAndStatusDisabledTest() throws Exception {
    Customer customer = new Customer(1, "victor", "tejada yau", "victoryau", "Panama",
        "vyau@gmail.com", "Panama city", ZonedDateTime.now(), Status.DISABLED);
    List<Customer> customers = new ArrayList<>();
    customers.add(customer);
    Query<Customer> query = Mockito.mock(Query.class);
    when(session.createQuery("from Customer where username = :username and status = :status")).thenReturn(query);
    when(query.setParameter("username", customer.getUsername())).thenReturn(query);
    when(query.setParameter("status", Status.DISABLED)).thenReturn(query);
    when(query.list()).thenReturn(customers);
    customerDaoImplementation.findCustomerByUserNameAndStatus(customer.getUsername(), Status.DISABLED.toString());
    assertEquals(customers.size(), 1);
  }
}