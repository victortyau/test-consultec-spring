package com.consultec.MyServerApplicantTestApplication.model;

public enum Status {
  ENABLED, 
  DISABLED
}