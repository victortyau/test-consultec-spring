package com.consultec.MyServerApplicantTestApplication.model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

@Entity
@Table(name = "customer", schema = "public")
public class Customer implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  
  @NotEmpty(message = "Please provide a name")
  @Column(name = "name")
  private String name;

  @Column(name = "lastname")
  private String lastName;

  @Column(name = "username")
  private String username;

  @Column(name = "password")
  private String password;

  @Column(name = "email")
  private String email;

  @Column(name = "address")
  private String address;

  @JsonFormat(pattern = "dd/MM/yyyy")
  @Column(name = "datecreated")
  private ZonedDateTime dateCreated;

  @Enumerated(EnumType.STRING)
  private Status status;


  public Customer() {
    super();
  }

  public Customer(long id,String name, String lastName, String username, String password, String email,
      String address, ZonedDateTime dateCreated, Status status) {
    this.id = id;
    this.name = name;
    this.lastName = lastName;
    this.username = username;
    this.password = password;
    this.email = email;
    this.address = address;
    this.dateCreated = dateCreated;
    this.status = status;
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getLastName() {
    return lastName;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getEmail() {
    return email;
  }

  public String getAddress() {
    return address;
  }

  public ZonedDateTime getDateCreated() {
    return dateCreated;
  }

  public Status getStatus() {
    return status;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setDateCreated(ZonedDateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  public void setStatus(Status status) {
    this.status = status;
  }
}
