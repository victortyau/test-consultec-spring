package com.consultec.MyServerApplicantTestApplication.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.consultec.MyServerApplicantTestApplication.dao.CustomerDao;
import com.consultec.MyServerApplicantTestApplication.model.Customer;
import com.consultec.MyServerApplicantTestApplication.model.Status;

@Service("customerService")
@Transactional
public class CustomerServiceImplementation implements CustomerService {
  
  @Autowired
  private CustomerDao customerDao;
  
  @Override
  public void saveCustomer(Customer customer) {
    // TODO Auto-generated method stub
    customerDao.saveCustomer(customer);
  }

  @Override
  public void deleteCustomer(long id) {
    // TODO Auto-generated method stub
    customerDao.deleteCustomer(id);
  }

  @Override
  public void updateCustomer(Customer customer) {
    // TODO Auto-generated method stub
    customerDao.updateCustomer(customer);
  }

  @Override
  public List<Customer> findAllCustomer() {
    // TODO Auto-generated method stub
    return customerDao.findAllCustomer();
  }

  @Override
  public Customer findCustomerById(long id) {
    // TODO Auto-generated method stub
    return customerDao.findCustomerById(id);
  }

  @Override
  public List<Customer> findCustomerByUserNameAndStatus(String username, String status) {
    // TODO Auto-generated method stub
    return customerDao.findCustomerByUserNameAndStatus(username, status);
  }
}