package com.consultec.MyServerApplicantTestApplication.dao;

import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractSession {
  
  @Autowired
  private EntityManager entityManager;

  protected Session getSession() {
    return entityManager.unwrap(Session.class);
  }
}
