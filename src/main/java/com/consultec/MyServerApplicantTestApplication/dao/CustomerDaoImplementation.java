package com.consultec.MyServerApplicantTestApplication.dao;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.consultec.MyServerApplicantTestApplication.model.Customer;
import com.consultec.MyServerApplicantTestApplication.model.Status;


@Repository
@Transactional
public class CustomerDaoImplementation extends AbstractSession implements CustomerDao {

  @Override
  public void saveCustomer(Customer customer) {
    // TODO Auto-generated method stub
    getSession().persist(customer);
  }

  @Override
  public void deleteCustomer(long id) {
    // TODO Auto-generated method stub
    Customer customer = findCustomerById(id);

    if (customer != null) {
      getSession().delete(customer);
    }
  }

  @Override
  public void updateCustomer(Customer customer) {
    // TODO Auto-generated method stub
    getSession().update(customer);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Customer> findAllCustomer() {
    // TODO Auto-generated method stub
    return (List<Customer>) getSession().createQuery("from Customer").list();
  }

  @Override
  public Customer findCustomerById(long id) {
    // TODO Auto-generated method stub
    return getSession().get(Customer.class, id);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Customer> findCustomerByUserNameAndStatus(String username, String status) {
    // TODO Auto-generated method stub
    Status mode = null;
    
    if (status.equals("ENABLED")) {
      mode = Status.ENABLED;
    }else {
      mode = Status.DISABLED;
    }
    
    return (List<Customer>) getSession()
        .createQuery("from Customer where username = :username and status = :status")
        .setParameter("username", username).setParameter("status", mode).list();
  }
}