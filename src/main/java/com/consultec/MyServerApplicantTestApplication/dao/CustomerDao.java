package com.consultec.MyServerApplicantTestApplication.dao;

import java.util.List;
import com.consultec.MyServerApplicantTestApplication.model.Customer;
import com.consultec.MyServerApplicantTestApplication.model.Status;


public interface CustomerDao {
  
  public void saveCustomer(Customer customer);
  
  public void deleteCustomer(long id);
  
  public void updateCustomer(Customer customer);
  
  public List<Customer> findAllCustomer();

  public Customer findCustomerById(long id);
  
  public List<Customer> findCustomerByUserNameAndStatus(String username, String status);
}