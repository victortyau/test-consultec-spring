package com.consultec.MyServerApplicantTestApplication.controller;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;
import com.consultec.MyServerApplicantTestApplication.model.Customer;
import com.consultec.MyServerApplicantTestApplication.service.CustomerService;
import util.CustomErrorType;


@Controller
@RequestMapping("/v1")
public class CustomerController {

  @Autowired
  private CustomerService customerService;

  // CREATE
  @RequestMapping(value = "/customers", method = RequestMethod.POST,
      headers = "Accept=application/json")
  public ResponseEntity<?> createCustomer(@RequestBody Customer customer,
      UriComponentsBuilder ucBuilder) {

    
    customer.setDateCreated(ZonedDateTime.now());
    customerService.saveCustomer(customer);
    HttpHeaders headers = new HttpHeaders();
    headers
        .setLocation(ucBuilder.path("/v1/customers/{id}").buildAndExpand(customer.getId()).toUri());

    return new ResponseEntity<String>(headers, HttpStatus.CREATED);
  }

  // DELETE
  @RequestMapping(value = "/customers/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id) {

    Customer customer = customerService.findCustomerById(id);

    if (customer == null) {
      return new ResponseEntity<>(
          new CustomErrorType("Unable to delete customer with id " + id + " not found."),
          HttpStatus.NOT_FOUND);
    }

    customerService.deleteCustomer(id);
    return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
  }

  // UPDATE
  @RequestMapping(value = "/customers/{id}", method = RequestMethod.PATCH)
  public ResponseEntity<?> updateCustomer(@PathVariable("id") Long id,
      @RequestBody Customer customer) {

    Customer current_customer = customerService.findCustomerById(id);

    if (current_customer == null) {
      return new ResponseEntity<>(
          new CustomErrorType("Unable to update customer with id " + id + " not found."),
          HttpStatus.NOT_FOUND);
    }

    current_customer.setName(customer.getName());
    current_customer.setLastName(customer.getLastName());
    current_customer.setUsername(customer.getUsername());
    current_customer.setPassword(customer.getPassword());
    current_customer.setEmail(customer.getEmail());
    current_customer.setStatus(customer.getStatus());

    customerService.updateCustomer(current_customer);
    return new ResponseEntity<Customer>(current_customer, HttpStatus.OK);
  }

  // GET ALL CUSTOMERS
  @RequestMapping(value = "/customers", method = RequestMethod.GET,
      headers = "Accept=application/json")
  public ResponseEntity<List<Customer>> getCustomers() {

    List<Customer> customers = new ArrayList<>();
    customers = customerService.findAllCustomer();

    if (customers.isEmpty()) {
      return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
    }

    return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
  }

  // GET BY ID
  @RequestMapping(value = "/customers/{id}", method = RequestMethod.GET)
  public ResponseEntity<Customer> getCustomerById(@PathVariable("id") Long id) {

    Customer customer = customerService.findCustomerById(id);

    if (customer == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<Customer>(customer, HttpStatus.OK);
  }

  // GET BY USERNAME AND STATUS
  @RequestMapping(value = "/customers/username", method = RequestMethod.GET,
      headers = "Accept=application/json")
  public ResponseEntity<List<Customer>> getCustomerByUsernameAndStatus(
      @RequestParam(value = "username", required = true) String username,
      @RequestParam(value = "status", required = true) String status) {
    
    List<Customer> customers = new ArrayList<>();
    customers = customerService.findCustomerByUserNameAndStatus(username, status );
    
    if (customers.isEmpty()) {
      return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
    }

    return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
  }
}
