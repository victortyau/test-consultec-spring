DROP TABLE IF EXISTS CUSTOMER;

CREATE TABLE CUSTOMER (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  lastname VARCHAR(100) NOT NULL,
  username VARCHAR(100) NOT NULL,
  password VARCHAR(100) NOT NULL,
  email VARCHAR(100) DEFAULT NULL,
  address VARCHAR(100) NOT NULL,
  datecreated TIMESTAMP,
  status VARCHAR(100) NOT NULL
);

INSERT INTO CUSTOMER (name, lastname, username, password, email, address, datecreated, status) 
VALUES 
('victor', 'tejada yau', 'victoryau', 'Panama', 'vyau@gmail.com', 'Panama city', now(),  'ENABLED'),
('manuela', 'Foster vega', 'mfoster', 'Colon', 'mfoster@gmail.com', 'Colon city', now() , 'ENABLED'),
('jose', 'Garrido', 'jgarrido', 'Los Santos', 'jgarrido@gmail.com', 'Las Tablas city', now(), 'ENABLED'),
('Alvaro', 'Pino', 'apino', 'Bocas del Todo', 'apino@gmail.com', 'Almirante city', now(), 'ENABLED'),
('Denia', 'Rodriguez', 'drodriguez', 'Herrera', 'drodriguez@gmail.com', 'Chitre city', now(), 'ENABLED'),
('Tina', 'Seelig', 'tseelig', 'Panama Oeste', 'tseelig@gmail.com', 'Chorrera city', now(), 'ENABLED'),
('Scott', 'Kelby', 'skelby', 'Veraguas', 'skelby@gmail.com', 'Santiago city', now(), 'ENABLED'),
('Charles', 'Platt', 'cplatt', 'Cocle', 'cplatt@gmail.com', 'Penonome city', now(), 'ENABLED'),
('Harry', 'Roberts', 'hroberts', 'Chiriqui', 'hroberts@gmail.com', 'Leeds city',now(), 'DISABLED'),
('Robert', 'Martin', 'rmartin', 'Darien', 'rmartin@gmail.com', 'California city',now(), 'DISABLED'),
('Annie', 'Leibovitz', 'aleibovitz', 'Photography', 'aleibovitz@gmail.com', 'Waterbury city',now(), 'DISABLED'),
('Linus', 'Torvalds', 'ltorvalds', 'Linux', 'ltorvalds@gmail.com', 'Helsinki',now(), 'DISABLED'),
('Peter', 'Dalmaris', 'pdalmaris', 'Kicad', 'pdalmaris@gmail.com', 'Auclkand', now(), 'DISABLED'),
('Kinneret', 'Yifrah', 'kyifrah', 'microcopy', 'kyifrah@gmail.com', 'TelAviv', now(), 'DISABLED'),
('Ann', 'Handley', 'ahandley', 'writers', 'ahandley@gmail.com', 'Boston', now(), 'DISABLED'),
('Antonio', 'Cangiano', 'acangiano', 'blogging', 'acangiano@gmail.com', 'Palermo' , now(), 'DISABLED');